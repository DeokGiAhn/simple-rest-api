### 빌드하기

```BASH
$ gradle build
```

### 실행하기

```BASH
$ gradle run
```