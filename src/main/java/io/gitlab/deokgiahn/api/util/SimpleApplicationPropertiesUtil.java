package io.gitlab.deokgiahn.api.util;

import io.gitlab.deokgiahn.api.util.configuration.ApplicationConfiguration;
import lombok.extern.slf4j.Slf4j;

import java.util.Properties;

@Slf4j
public class SimpleApplicationPropertiesUtil {
  public static String getProperties(final String key) {
    final ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration();
    final Properties properties = applicationConfiguration.getMessageProperties();
    return properties.getProperty(key);
  }
}

