package io.gitlab.deokgiahn.api.util.configurer;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/api")
public class ApplicationConfigurer extends ResourceConfig {
  public ApplicationConfigurer() {
    packages(true, this.getClass().getPackage().getName());
  }
}
