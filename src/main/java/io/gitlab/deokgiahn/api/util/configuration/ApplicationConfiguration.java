package io.gitlab.deokgiahn.api.util.configuration;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Properties;

@Slf4j
public class ApplicationConfiguration {
  private static final String CONFIGURATION_FILE = "application.properties";

  public Properties getMessageProperties() {
    final Properties properties = new Properties();
    final ClassLoader classLoader = this.getClass().getClassLoader();

    try (final InputStream stream = classLoader.getResourceAsStream(CONFIGURATION_FILE)) {
      final BufferedInputStream bufferedInputStream = new BufferedInputStream(stream);
      properties.load(bufferedInputStream);
    } catch (final Exception e) {
      System.out.println(e);
    }

    return properties;
  }
}

