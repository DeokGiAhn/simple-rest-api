package io.gitlab.deokgiahn.api.account;

import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Slf4j
@Path("/accounts")
public class AccountController {
  @GET
  public Response findAll() {
    System.out.println("OK~~~~");
    return Response.status(Response.Status.OK)
            .type(MediaType.TEXT_PLAIN_TYPE)
            .encoding("UTF-8")
            .entity("OK")
            .build();
  }
}
