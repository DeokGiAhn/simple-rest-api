package io.gitlab.deokgiahn.api;

import io.gitlab.deokgiahn.api.util.SimpleApplicationPropertiesUtil;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.startup.Tomcat;

import javax.servlet.ServletException;
import java.io.File;
import java.net.MalformedURLException;

public class ApplicationInitializer {
  public static void main(final String... args)
          throws ServletException, LifecycleException, MalformedURLException {
    new ApplicationInitializer().start();
  }

  private void start() throws ServletException, LifecycleException, MalformedURLException {
    final Tomcat tomcat = new Tomcat();

    final Connector connector = getConnector();

    tomcat.getService().addConnector(connector);

    final String webAppDirLocation = "src/main/webapp/";
    final Context context = tomcat.addWebapp("/", new File(webAppDirLocation).getAbsolutePath());
    final File configFile = new File(webAppDirLocation + "WEB-INF/web.xml");
    context.setConfigFile(configFile.toURI().toURL());


    tomcat.start();
    tomcat.getServer().await();
  }

  private static Connector getConnector() {
    final Connector connector = new Connector();

    connector.setURIEncoding("UTF-8");
    connector.setAttribute("protocol", "HTTP/1.1");
    connector.setPort(Integer.parseInt(SimpleApplicationPropertiesUtil.getProperties("web_port")));

    return connector;
  }
}
